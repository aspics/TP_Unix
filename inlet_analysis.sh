#!/bin/bash

# Author : Paolo Errante, PfD, LMFA 

# remove old files
file=`ls . | grep "droplet_verif"`
for old in $file; do 
  rm -f $old
   echo "clean old $old file"
done

# setup output dir
dir='comparisons'
if [ ! -d $dir ]; then
   mkdir $dir
   echo "output dir $dir created"
fi


#Loops over radial positions to calculate mean and rms of axial and velocities + droplet D10 diameter at each position
for r in 0 0.00063 0.0012705 0.0019005 0.002541 0.003171 0.0038115 0.0044415 0.005082; do
  count=$(awk -v r=$r '{if($2==r) sum += 1}END{print sum}' injection_SP2.inj) #counts the number of droplets
  mean=$(awk -v r=$r -v count=$count '{if($2==r) sum += $4}END{print sum/count}' injection_SP2.inj) #calculate mean
  rms=$(awk -v r=$r -v count=$count -v mean=$mean '{if($2==r) sum += (mean - $4)^2}END{print sqrt(sum/count)}' injection_SP2.inj) #calculate rms
  echo "$r $mean $rms" >> droplet_verif_u.txt #print results to a file
done

for r in 0 0.00063 0.0012705 0.0019005 0.002541 0.003171 0.0038115 0.0044415 0.005082; do
  count=$(awk -v r=$r '{if($2==r) sum += 1}END{print sum}' injection_SP2.inj)
  mean=$(awk -v r=$r -v count=$count '{if($2==r) sum += $5}END{print sum/count}' injection_SP2.inj)
  rms=$(awk -v r=$r -v count=$count -v mean=$mean '{if($2==r) sum += (mean - $5)^2}END{print sqrt(sum/count)}' injection_SP2.inj)
  echo "$r $mean $rms" >> droplet_verif_v.txt
done

for r in 0 0.00063 0.0012705 0.0019005 0.002541 0.003171 0.0038115 0.0044415 0.005082; do
  count=$(awk -v r=$r '{if($2==r) sum += 1}END{print sum}' injection_SP2.inj)
  mean=$(awk -v r=$r -v count=$count '{if($2==r) sum += $7}END{print sum/count}' injection_SP2.inj)
  rms=$(awk -v r=$r -v count=$count -v mean=$mean '{if($2==r) sum += (mean - $7)^2}END{print sqrt(sum/count)}' injection_SP2.inj)
  echo "$r $mean $rms" >> droplet_verif_d.txt
done


column -t droplet_verif_u.txt > $dir/droplet_u_inlet.txt
column -t droplet_verif_v.txt > $dir/droplet_v_inlet.txt
column -t droplet_verif_d.txt > $dir/droplet_d_inlet.txt
