# Atelier Unix

Unix est un système d'exploitation 

- interactif (mais avec traitement en batch possible)
- multi-tâches (concurrentes et indépendantes)
- multi-utilisateurs (avec un système d'identification et de droits d'accès différenciés aux fichiers)

Unix donne accès à 

- un interpréteur de commandes (shell)
- un gestionnaire de processus (pipes)
- de l'intégration au réseau (partage de ressources, applications réparties...)


## Table des matières

<!-- vim-markdown-toc GitLab -->

* [Introduction aux commandes Unix](#introduction-aux-commandes-unix)
  * [Ouvrir un terminal](#ouvrir-un-terminal)
  * [Commandes de base](#commandes-de-base)
    * [Naviguer dans les répertoires](#naviguer-dans-les-répertoires)
    * [Manipulation de fichiers](#manipulation-de-fichiers)
  * [Enchaîner plusieurs commandes](#enchaîner-plusieurs-commandes)
    * [Entrée/Sortie](#entréesortie)
  * [Trouver un fichier](#trouver-un-fichier)
  * [Commandes traitant le contenu des fichiers de type texte](#commandes-traitant-le-contenu-des-fichiers-de-type-texte)
    * [Lecture](#lecture)
    * [Écriture et édition](#écriture-et-édition)
  * [Gestion des processus](#gestion-des-processus)
  * [Filtres sed et awk](#filtres-sed-et-awk)
* [Bash](#bash)
    * [Exercices de base](#exercices-de-base)
    * [Exercices sur les structures de contrôle](#exercices-sur-les-structures-de-contrôle)
    * [Exercices sur les fonctions](#exercices-sur-les-fonctions)
* [Connexion distante](#connexion-distante)
  * [ssh](#ssh)

<!-- vim-markdown-toc -->

# Introduction aux commandes Unix

## Ouvrir un terminal

- Chercher dans les menus le logiciel Terminal ou Console pour ouvrir un terminal. 
- Ouvrir un terminal par un clic droit sur le bureau. 
- Ouvrir 2 terminaux. En fermer un avec la souris, l'autre au clavier. 

<!--ba-->
```bash
  ~$ exit
``` 
<!--ea-->


## Commandes de base

L'accès au manuel d'usage des commandes se fait par la commande **man**. 

<!--ba-->
```
  ~$ man man
```
<!--ea-->

- Quel est d'après le manuel le descriptif (NAME) de cette commande ?

<!--ba-->
```bash
  man - an interface to the on-line reference manuals
  man - Interface de consultation des manuels de référence en ligne
``` 
<!--ea-->

- Cherchez l'usage de **date**, **whoami**, **hostname**, **who**, **echo**, **id**, **uname**. 

- Quel jour sommes-nous ? Uilisez la commande **date**.

<!--ba-->
```bash
  ~$ date
```
<!--ea-->

- Quel jour de la semaine êtes-vous né ? Utilisez la commande **cal**.

<!--ba-->
```bash
Alan Turing est né le 23 juin 1912. 

  ~$ cal 6 1912

C\'était un dimanche.
```
<!--ea-->

- Où se trouve la commande **man** ? 

<!--ba-->
```bash
  ~$ whereis man
```
<!--ea-->

- Quelle commande **man** utilisez-vous ?

<!--ba-->
```bash
  ~$ which man
```
<!--ea-->


- Vous venez d'ouvrir un terminal. Dans quel répertoire vous trouvez-vous ?

<!--ba-->
```bash
À la racine de mon compte.

  ~$ pwd
``` 
<!--ea-->

- Exécutez les commandes suivantes :

```bash

  ~$ pwd

  ~$ cd /tmp

  ~$ pwd

  ~$ cd 
```

Où êtes-vous ?

<!--ba-->
```bash
À la racine de mon compte.
```
<!--ea-->

- Affichez le contenu des variables définissant votre répertoire à la racine de votre compte et celui où vous êtes actuellement 

<!--ba-->
```bash
  ~$ echo $HOME

  ~$ echo ~

  ~$ echo $PWD
```
<!--ea-->

- Sous quel shell travaillez-vous ?

<!--ba-->
```bash
  ~$ echo $SHELL
```
<!--ea-->

- Quel est le nom de votre machine ?

<!--ba-->
```bash
  ~$ uname -n

  ~$ hostname

  ~$ echo $HOSTNAME
```
<!--ea-->

- Quel est votre login ?

<!--ba-->
```bash
  ~$ whoami

  ~$ echo $USER
```
<!--ea-->

- À quels groupes unix appartenez-vous ?

<!--ba-->
```bash
  ~$ groups

  ~$ id
```
<!--ea-->

- Quel est votre numéro d'utilisateur ?

<!--ba-->
```bash
  ~$ id

  ~$ echo $UID
```
<!--ea-->

### Naviguer dans les répertoires

- Créez un répertoire de nom UNIX à la racine. Y créer le dossier TP1. Descendez dans ce répertoire TP1.

<!--ba-->
```bash
  ~$ mkdir ~/UNIX

  ~$ mkdir ~/UNIX/TP1

  ~$ cd ~/UNIX/TP1

  ~/UNIX/TP1$ pwd
```
<!--ea-->

- Remontez d'un répertoire pour vous retrouver dans le répertoire UNIX. 

<!--ba-->
```bash
  ~/UNIX/TP1$ cd ..
  
  ~/UNIX$ pwd
```
<!--ea-->

- Placez-vous à la racine et faites un aller-retour dans le répertoire TP1 en une seule ligne.

<!--ba-->
```bash
  ~/UNIX$ cd; cd ~/UNIX/TP1; cd
```
<!--ea-->

- Créez le répertoire TP2 dans UNIX. Créez un dossier exo1 dans les répertoires TP1 et TP2.

<!--ba-->
```bash
  ~$ mkdir ~/UNIX/TP1/exo1

  ~$ mkdir ~/UNIX/TP2; mkdir ~/UNIX/TP2/exo1
```
<!--ea-->

- Placez-vous dans le répertoire exo1 de TP2 et faites un aller-retour dans le répertoire exo1 de TP1.

<!--ba-->
```bash
  ~$ cd ~/UNIX/TP1/exo1

  ~/UNIX/TP1/exo1$ pushd ../../TP2/exo1
  ~/UNIX/TP2/exo1$ pushd
```
<!--ea-->

### Manipulation de fichiers

- Placez-vous dans à votre racine. Listez le contenu du répertoire UNIX avec la commande **ls**.

<!--ba-->
```bash
  ~$ ls -l ~/UNIX
```
<!--ea-->

- Affichez l'aborescence du répertoire UNIX à l'aide de la commande **tree**.

<!--ba-->
```bash
  ~$ tree ~/UNIX
```
<!--ea-->

- Listez les fichers ou dossiers cachés.  

<!--ba-->
```bash
  ~$ ls -la ~/UNIX
```
<!--ea-->

- Détruisez le répertoire exo1 de TP2 de deux manières différentes

<!--ba-->
```bash
  ~$ rm -r ~/UNIX/TP2/exo1

ou 

  ~$ rmdir ~/UNIX/TP2/exo1
```
<!--ea-->

- Descendez dans le répertoire UNIX. Copiez TP1 dans un nouveau répertoire TP1bis à l'aide de la commande **cp**.

<!--ba-->
```bash
  ~/UNIX$ cp -r TP1 TP1bis

ou

  ~/UNIX$ cp -R TP1 TP1bis
```
<!--ea-->

- Copiez à nouveau TP1 dans TP1bis. Que se passe-t'il ?

<!--ba-->
```bash
  ~/UNIX$ cp -r TP1 TP1bis
  ~/UNIX tree
	.
	├── TP1
	│   └── exo1
	├── TP1bis
	│   ├── exo1
	│   └── TP1
	│       └── exo1
	└── TP2

Le répertoire TP1 a été copié dans TP1bis car TP1bis existait déjà.
```
<!--ea-->

- Créez un fichier essai.txt dans le répertoire TP2 par la commande **touch**

<!--ba-->
```bash
  ~/UNIX$ touch TP2/essai.txt
```
<!--ea-->

- Copier le répertoire TP2 dans un nouveau répertoire, TP2bis, en préservant la date actuelle de TP2 avec la commande **cp**.


<!--ba-->
```bash
  ~/UNIX$ cp -pr TP2 TP2bis
```
<!--ea-->

- Copier le répertoire TP2 dans un nouveau répertoire, TP3ter, en préservant la date actuelle de TP2 avec la commande **rsync**.

<!--ba-->
```bash
  ~/UNIX$ rsync -av TP2/ TP3ter/
```
<!--ea-->

- Renommer TP3ter en TP2ter à l'aide de la commande **mv**.

<!--ba-->
```bash
  ~/UNIX$ mv TP3ter/ TP2ter/
```
<!--ea-->

- Afficher tous les dossiers commençant par TP2.

<!--ba-->
```bash
  ~/UNIX$ ls TP2*

ou 

  ~/UNIX$ tree TP2*
```
<!--ea-->

- Afficher la liste des répertoires par ordre alphabétique puis par ordre inversé de l'ordre alphabétique. 

<!--ba-->
```bash
  ~/UNIX$ ls -l

  ~/UNIX$ ls -lr
```
<!--ea-->

- Afficher la liste des répertoires du plus récent au plus ancien puis inversement.

<!--ba-->
```bash
  ~/UNIX$ ls -lt

  ~/UNIX$ ls -lrt
```
<!--ea-->

- Afficher la taille des répertoires en bloc et en kilo octet.

<!--ba-->
```bash
  ~/UNIX$ ls -s

  ~/UNIX$ ls -l

  ~/UNIX$ ls -lh
```
<!--ea-->

- Supprimer tous les répertoires de UNIX. **ATTENTION : pas de "corbeille"**

<!--ba-->
```bash
  ~/UNIX$ rm -fr *
```
<!--ea-->


- Afficher le contenu du répertoire /etc/dictionaries-common

```bash
  ~$ ls -l /etc/dictionaries-common
```
<!--ea-->

- À qui appartient le fichier nommé words ?

<!--ba-->
```bash
  à root
```
<!--ea-->

- Quel type de fichier est-ce ?

<!--ba-->
```bash
  un fichier ascii, alias qui pointe sur /usr/share/dict/american-english (chez moi)
```
<!--ea-->

- Qui a les droits en lecture sur ce fichier ? 

<!--ba-->
```bash
  tout le monde.
```
<!--ea-->

- Créer le fichier **file** et le répertoire **dir** à la racine de votre compte. Quels sont les droits sur ce fichier et ce répertoire ?

<!--ba-->
```bash
  ~$ touch file
  ~$ mkdir dir
  ~$ ls -ld file dir
drwxrwsr-x 2 acadiou acadiou 4096 déc.  27 11:57 dir
-rw-rw-r-- 1 acadiou acadiou    0 déc.  27 11:57 file
```
<!--ea-->

- Changer les permissions sur le fichier pour ne permettre la lecture et l'écriture qu'à vous seul (utiliser **chmod**).

<!--ba-->
```bash
  ~$ chmod 600 file
ou 
  ~$ chmod g-rw,o-r file
  ~$ ls -ld file dir
drwxrwsr-x 2 acadiou acadiou 4096 déc.  27 11:57 dir
-rw------- 1 acadiou acadiou    0 déc.  27 11:58 file
```
<!--ea-->

- Changer les permissions par défaut sur les fichiers et répertoires (utiliser **umask**)

<!--ba-->
```bash
  ~$ umask 022
  ~$ touch file
  ~$ mkdir dir
  ~$ ls -ld file dir
drwxr-sr-x 2 acadiou acadiou 4096 déc.  27 12:05 dir
-rw-r--r-- 1 acadiou acadiou    0 déc.  27 12:05 file
  ~$
  ~$ umask 077
  ~$ touch file
  ~$ mkdir dir
  ~$ ls -ld file dir
drwx--S--- 2 acadiou acadiou 4096 déc.  27 12:06 dir
-rw------- 1 acadiou acadiou    0 déc.  27 12:06 file
`
```
<!--ea-->


## Enchaîner plusieurs commandes

- Chaîner en une seule ligne la descente dans le répertoire **/tmp**, l'affichage des fichiers qu'il contient et la remontée au répertoire de départ, quel que soit le succès de la réalisation de ces différentes commandes

<!--ba-->
```bash
   ~/UNIX$ cd /tmp ; ls -l ; cd -
```
<!--ea-->

- Modifiez la syntaxe pour ajouter la condition suivant laquelle l'enchaînement ne se fait que si la commande précédente s'est correctement effectuée

<!--ba-->
```bash
   ~/UNIX$ cd /tmp && ls -l && cd -
```
<!--ea-->

- Traiter le cas où  la commande précédente ne s'est pas correctement effectuée

<!--ba-->
```bash
   ~/UNIX$ cd /tmp && ls -l || cd -
```
<!--ea-->


- Afficher le code de retour de la commande **ls -l** 

<!--ba-->
```bash
  ~/UNIX$ ls -l  ; echo $?
0 si le processus s'est déroulé correctement
  ~/UNIX$ ls -l no* ; echo $?
une autre valeur sinon
```
<!--ea-->



### Entrée/Sortie

Chaque commande a une **entrée standard**, une **sortie standard** et une **sortie d'erreur**.  Par défaut, l'entrée standard est le clavier, la sortie standard est l'écran, et la sortie d'erreur est aussi l'écran. Cela peut être modifié par des **redirections**. 

- Redirigez la sortie standard de la commande ls -l vers le fichier res.txt. Vous pouvez en visualiser le contenu avec la commande **cat**, explicitée à la section suivante.

<!--ba-->
```bash
  ~UNIX$ ls -l > res.txt
  ~UNIX$ cat res.txt
``` 
<!--ea-->

- Quelles est la différence entre 

```bash
  ~UNIX$ ls -l > res.txt
``` 

et

```bash
  ~UNIX$ ls -l >> res.txt
``` 

<!--ba-->
```bash
  > redirige la sortie en écrasant le contenu du fichier initial alors que >> ajoute la sortie de la commande à la fin du fichier initial.
``` 
<!--ea-->

- À l'aide de la commande **cat** et des chevron d'entrée et sortie, copiez le contenu de res.txt dans un nouveau fichier new.txt

<!--ba-->
```bash
  ~UNIX$ cat > new.twt < res.txt
``` 
<!--ea-->

- Essayez de faire afficher la liste de fichiers qui n'exitent pas (par exemple commençant par no). Que se passe-t'il ?

<!--ba-->
```bash
  ~UNIX$ ls -l no*
ls: cannot access 'no*': No such file or directory

Un message d'erreur s'affiche à l'écran. 
``` 
<!--ea-->

- Recommencez en redirigeant la sortie standard vers le fichier res.txt

<!--ba-->
```bash
  ~UNIX$ ls -l no* > res.txt
ls: cannot access 'no*': No such file or directory

Un message d'erreur s'affiche à l'écran. 
Le fichier res.txt est vide
``` 
<!--ea-->

- Recommencez en redirigeant la sortie standard et la sortie d'erreur vers res.txt

<!--ba-->
```bash
  ~UNIX$ ls -l no* > test.txt 2>&1

Rien ne s'affiche à l'écran mais test.txt contient le message d'erreur 
ls: cannot access 'no*': No such file or directory
``` 
<!--ea-->

- Recommencez en affichant un message si vous en avez trouvé

<!--ba-->
```bash
  ~UNIX$ ls -l no* > test.txt 2>&1 && echo "Found!"
``` 
<!--ea-->

- Recommencez en affichant un message si vous n'en avez pas trouvé

<!--ba-->
```bash
  ~UNIX$ ls -l no* > test.txt 2>&1 || echo "Not Found!"
``` 
<!--ea-->



## Trouver un fichier

- Chercher tous les fichiers contenant **bashrc** avec la commande **locate**

<!--ba-->
```bash
   ~/UNIX$ locate bashrc
```
<!--ea-->


- Avec la commande **find**, rechercher les fichiers dont l'extension est **.pdf**, contenus dans le répertoire Documents 

<!--ba-->
```bash
  ~/UNIX$ find ~/Documents/. -name '*.pdf' 
```
<!--ea-->

- Recommencer en affichant les droits sur ces fichiers et sa taille en format M, K etc.

<!--ba-->
```bash
  ~/UNIX$ find ~/Documents/. -name '*.pdf' -exec ls -lsh {} \; 
```
<!--ea-->

- Recommencer en affichant les droits sur ces fichiers et sa taille en format M, K etc. et en les triant du plus petit au plus gros. 

<!--ba-->
```bash
  ~/UNIX$ find ~/Documents/. -name '*.pdf' -exec ls -lsh {} \; |sort -n  
```
<!--ea-->

- Recommencer en affichant les droits sur ces fichiers et sa taille en format M, K etc. et en affichant les 10 plus gros par ordre croissant

<!--ba-->
```bash
  ~/UNIX$ find ~/Documents/. -name '*.pdf' -exec ls -lsh {} \; |sort -n |tail -n 10
```
<!--ea-->

- Trouver tous les fichiers de ce répertoire  de taille plus grande que 500M et les afficher par taille croissante

<!--ba-->
```bash
  ~/UNIX$ find ~/Documents/ -size +500M -exec du -sh {} \; |sort -nr
```
<!--ea-->



## Commandes traitant le contenu des fichiers de type texte

### Lecture

- Descendre dans le répertoire /etc/dictionaries-common et afficher le fichier words à l'aide des commandes **cat**, **less** et **more**. 

- À l'aide de la commande **less** chercher les mots commençant par Sco

<!--ba-->
```bash
  /etc/dictionaries-common$ less words

puis tapez /Sco

quittez less en tapant q
```
<!--ea-->

- Combien de lignes comporte ce fichier en tout ? Utilisez la commande **wc**.

<!--ba-->
```bash
  /etc/dictionaries-common$ wc -l 
  99171 words
```
<!--ea-->

- Combien de lignes comprenant les charactères Sco il y a t'il dans ce fichier ? Utilisez les commandes **grep** et **wc**

<!--ba-->
```bash
  /etc/dictionaries-common$ grep Sco words |wc -l
  35 words
```
<!--ea-->

- Combien de lignes comprenant les charactères Sco ou sco il y a t'il dans ce fichier ?

<!--ba-->
```bash
  /etc/dictionaries-common$ grep -i Sco words |wc -l
  456 words
```
<!--ea-->

- Retournez dans le répertoire UNIX crée précédemment à la racine de votre compte. Copiez-y les fichier words de  /etc/dictionaries-common.

<!--ba-->
```bash
  /etc/dictionaries-common$ cd ~/UNIX
  ~/UNIX$ cp /etc/dictionaries-common/words .
```
<!--ea-->

- À qui appartient ce fichier ?

<!--ba-->
```bash
  à vous. 
```
<!--ea-->

- Quels sont les droits unix sur ce fichier ?

<!--ba-->
```bash
  -rw-r--r--

  l'utilisateur peut lire et écrire, le groupe et les autres peuvent lire le fichier. 
```
<!--ea-->

- Supprimer les droits en lecture des autres

<!--ba-->
```bash
  ~/UNIX$ chmod o-r words

ou 

  ~/UNIX$ chmod 640 words
```
<!--ea-->

- Faire un lien symbolique de ce fichier vers un nouveau fichier nommé mots.

<!--ba-->
```bash
  ~/UNIX$ ln -s words mots

  ~/UNIX$ ls -l 
```
<!--ea-->

- Identifier le type du fichier avec la commande **file**. 

<!--ba-->
```bash
  ~/UNIX$ file words 
words: UTF-8 Unicode text
  ~/UNIX$ file mots
mots: symbolic link to words
```
<!--ea-->



- Afficher les 3 premiers mots de la liste words puis les 5 derniers à l'aide des commandes **head** et **tail**.

<!--ba-->
```bash
  ~/UNIX$ head -3 words

  ~/UNIX$ tail -5 words
```
<!--ea-->

### Écriture et édition

- Rediriger les 3 premiers mots de la liste words dans un nouveau fichier nommé extrait

<!--ba-->
```bash
  ~/UNIX$ head -3 words > extrait

  ~/UNIX$ cat extrait
```
<!--ea-->

- Ajouter les 5 derniers mots de words au fichier extrait.

<!--ba-->
```bash
  ~/UNIX$ tail -5 words >> extrait

  ~/UNIX$ cat extrait
```
<!--ea-->

- Ajouter le mot FIN à la fin du fichier mots avec la commande **cat**.

<!--ba-->
```bash
  ~/UNIX$ cat << EOF >> mots
  FIN
  EOF
  ~/UNIX$ tail mots
```
<!--ea-->

- Que se passe-t'il sur la liste words ?

<!--ba-->
```bash
  Elles est également modifiée car le fichier mots est un alias de words.
```
<!--ea-->

- Rediriger les 3 premières lignes de la liste words dans un nouveau fichier nommé extrait et les 5 premières lignes dans un nouveau fichier nommé extrait2

<!--ba-->
```bash
  ~/UNIX$ head -3 words > extrait
  ~/UNIX$ head -5 words > extrait2
```
<!--ea-->


- Afficher les différences entre les fichiers extrait et extrait2 avec la commande **diff**, **vimdiff** ou **meld**.

<!--ba-->
```bash
  ~/UNIX$ diff extrait extrait2
3a4,5
> AB's
> ABM's
```
<!--ea-->


- Créer avec la commande **cat** un fichier nommé file.txt contenant les lignes 

```bash
  Ce fichier 
  contient 2 lignes.
```

<!--ba-->
```bash
  ~/UNIX$ cat > file.txt
  Ce fichier 
  contient 2 lignes.
  ^D
  ~/UNIX$ cat file.txt
```
<!--ea-->

- Créer un autre fichier nommé fileNum.txt contenant les mêmes lignes mais numérotées. 

<!--ba-->
```bash
  ~/UNIX$ cat -n > fileNum.txt < file.txt
  ~/UNIX$ cat fileNum.txt
```
<!--ea-->

- Éditer le fichier file.txt avec **vi**, **nano** et **gedit**. Ajouter le mot **fin**, sauver et quitter l'éditeur. 

## Gestion des processus

- Afficher les processus qui tournent sur votre machine avec **ps**

<!--ba-->
```bash
  ~$ ps -ef
```
<!--ea-->

- Lancer thunderbird depuis un terminal et détacher le process par la commande **CTRL+Z**. Le mettre en tâche de fond.

<!--ba-->
```bash
  ~$ thunderbird
[calBackendLoader] Using Thunderbird's builtin libical backend
^Z
[1]+  Stopped                 thunderbird
  ~$ bg
[1]+ thunderbird &
```
<!--ea-->

- Afficher les processus en tâche de fond avec la commande **jobs**.

<!--ba-->
```bash
  ~$ jobs
[1]+  Running                 thunderbird &
```
<!--ea-->


- Renvoyer le processus de thunderbird au premier plan et interrompre le processus par la commande **CTRL+C**. 

<!--ba-->
```bash
  ~$ fg 
ou
  ~$ fg %1
^C
```
<!--ea-->


- Relancer thunderbird en tâche de fond en détachant le processus de la fenêtre en ajoutant un **&** à la fin de la commande.

<!--ba-->
```bash
  ~$ thunderbird &
```
<!--ea-->


- Rechercher le processus associé et l'interrompre par la commande **kill**

<!--ba-->
```bash
  ~$ ps -ef|grep thun
```
<!--ea-->


- Relancer thunderbird puis afficher les processus qui tournent avec **top**. Rechercher le PID du processus associé à thunderbird (**L**) puis tuer thunderbird dans **top**. Utiliser l'aide (en tapant **h**).

<!--ba-->
```bash
  ~$ top
h permet d'afficher l'aide
L permet d'entrer le nom à chercher : thun
k suivi du numero de PID trouvé interromp le processus
```
<!--ea-->


- Relancer thunderbird puis afficher les processus qui tournent avec **htop**. Rechercher le PID du processus associé à thunderbird (**F3**) puis tuer thunderbird dans **htop**. 

<!--ba-->
```bash
  ~$ htop
F3 permet d'entrer le nom à chercher : thun
envio de SIGTERM permet d'interrompre le processus
```
<!--ea-->

- Mettre thunderbird en tâche de fond avec **&** et affichier le nom du terminal (TTY).

<!--ba-->
```bash
  ~$ thunderbird &
  ~$ ps -A|grep thun
31692 pts/23   00:00:35 thunderbird

le processus est ici associé au terminal pts/23.
```
<!--ea-->


- Mettre thunderbird en tâche de fond avec **&** et quitter le terminal. Vérifier avec **top** ou **ps -ef|grep thun** que thunderbird a été interrompu. 

<!--ba-->
```bash
  ~$ thunderbird &
  ~$ ps -A|grep thun
31692 pts/23   00:00:35 thunderbird
  ~$ exit
  ~$ ps -A|grep thun

```
<!--ea-->


- Mettre thunderbird en tâche de fond avec **nohup** et quitter le terminal. Vérifier avec **top** ou **ps -ef|grep thun** que thunderbird tourne toujours.


<!--ba-->
```bash
  ~$ nohup thunderbird &
  ~$ ps -A|grep thun
32191 pts/22   00:00:12 thunderbird
  ~$ exit
  ~$ ps -A|grep thun
32191 ?        00:00:14 thunderbird
```
<!--ea-->


- **nohup** ne peut lancer qu'une commande à la fois. Pour ouvrir plusieurs sessions, utiliser un multiplicateur de terminal comme **screen** ou **tmux**. Ouvrir une nouvelle session **tmux** nommée **Essai**. Afficher la session et la détacher.

<!--ba-->
```bash
  ~$ tmux new -s Essai
  ~$ tmux ls
^bd
[detached (from session Essai)]
```
<!--ea-->

- Réattacher la session **Essai**.
<!--ba-->
```bash
  ~$ tmux attach -t Essai
```
<!--ea-->

- Terminer la session **Essai**.
<!--ba-->
```bash
  ~$ tmux kill-session -t Essai
  ~$ tmux ls
no server running on /tmp/tmux-1000/default
```
<!--ea-->


## Filtres sed et awk

- Ressources externes pour les exemples 

  * [texte](texte.txt)
  * [liste (fichier csv)](liste.csv)
  * [données en colonnes](injection_SP2.inj) et script [awk](inlet_analysis.sh) (contribution grâcieuse de Paolo Errante)
  * fichier de [log](job.out) et script [awk](norme.awk)

- Afficher le contenu du fichier texte.txt par la commande **sed**

<!--ba-->
```bash
  ~$ sed '' texte.txt
Ceci est la première ligne du fichier texte.txt
C'est un texte avec des ereurs.
Beaucoups d'ereurs et encore des ereurs.
Une ligne ne contenant pas d'erreurs.
Ceci est la dernière ligne du fichier texte.txt
```
<!--ea-->

- Afficher les lignes de ce fichier dans lesquelles apparaît le contenu du fichier texte.txt par la commande **sed**

<!--ba-->
```bash
  ~$ sed -n '/ereur/p' texte.txt 
C'est un texte avec des ereurs.
Beaucoups d'ereurs et encore des ereurs.
```
<!--ea-->

- Afficher les lignes commençant par **C**.

<!--ba-->
```bash
  ~$ sed -n '/^C/p' texte.txt
Ceci est la première ligne du fichier texte.txt
C'est un texte avec des ereurs.
Ceci est la dernière ligne du fichier texte.txt
```
<!--ea-->


- Afficher les lignes commençant par **C** et se terminant par **txt**.

<!--ba-->
```bash
  ~$ sed -n '/^C.*txt$/p' texte.txt
Ceci est la première ligne du fichier texte.txt
Ceci est la dernière ligne du fichier texte.txt
```
<!--ea-->

- Remplacer dans chaque ligne la première occurrence du mot **ereur** par **erreur**.

<!--ba-->
```bash
  ~$ sed -n 's/ereur/erreur/p' texte.txt
C'est un texte avec des erreurs.
Beaucoups d'erreurs et encore des ereurs.
```
<!--ea-->


- Remplacer dans tout le texte le mot **ereur** par **erreur**.

<!--ba-->
```bash
  ~$ sed -e 's/ereur/erreur/g' texte.txt > texte_corrige.txt
  ~$ cat texte_corrige.txt
Ceci est la première ligne du fichier texte.txt
C'est un texte avec des erreurs.
Beaucoups d'erreurs et encore des erreurs.
Une ligne ne contenant pas d'erreurs.
Ceci est la dernière ligne du fichier texte.txt
```
<!--ea-->


- Remplacer sur place dans tout le texte le mot **ereur** par **erreur**.

<!--ba-->
```bash
  ~$ sed -i 's/ereur/erreur/g' texte.txt
  ~$ cat texte.txt
Ceci est la première ligne du fichier texte.txt
C'est un texte avec des erreurs.
Beaucoups d'erreurs et encore des erreurs.
Une ligne ne contenant pas d'erreurs.
Ceci est la dernière ligne du fichier texte.txt
```
<!--ea-->


- Supprimer les lignes contenant le mot **ligne**

<!--ba-->
```bash
  ~$ sed -e '/ligne/d' texte.txt
C'est un texte avec des erreurs.
Beaucoups d'erreurs et encore des erreurs.
```
<!--ea-->

- Afficher les lignes contenant le caractère **'**

<!--ba-->
```bash
  ~$ sed -n "/'/p"  texte.txt
C'est un texte avec des erreurs.
Beaucoups d'erreurs et encore des erreurs.
Une ligne ne contenant pas d'erreurs.
```
<!--ea-->

- Supprimer toutes les lignes à partir de la troisième.

<!--ba-->
```bash
  ~$ sed  '3,$d'  texte.txt
Ceci est la première ligne du fichier texte.txt
C'est un texte avec des erreurs.
```
<!--ea-->

- Supprimer les 3 derniers chiffres d'une chaîne numérique

<!--ba-->
```bash
  ~$ echo "12345" | sed -e 's/[0-9]\{3\}$//'
12
```
<!--ea-->

- Supprimer le premier caractère d'une chaîne et passer le reste en majuscules

<!--ba-->
```bash
  ~$ echo "abcde" | sed -e "s/^[a-zA-Z]\{1\}//" | tr '[:lower:]' '[:upper:]'
BCDE
```
<!--ea-->

- Lister les fichiers de **/usr/bin** dont l'avant-dernière lettre est un **a**

<!--ba-->
```bash
  ~$ ls /usr/bin | sed -n '/a.$/p'
a5toa4
ab
aclocal
acroread
alsabat
(...)
```
<!--ea-->

- Écrire un script awk qui affiche la sortie d'une commande

<!--ba-->
```bash
  ~$ cat > print-awk.sh << EOF
  awk '{ print }'
  EOF
  ~$ chmod 755 print-awk.sh
  ~$ echo coucou | ./print-awk.sh
coucou
```
<!--ea-->

- Afficher la liste des utilisateurs (en utilisant /etc/passwd)
<!--ba-->
```bash
  ~$ awk -F: '{print $1}' /etc/passwd 
```
<!--ea-->

- Afficher la dernière ligne du fichier en utilisant BEGIN...END

<!--ba-->
```bash
  ~$ awk 'BEGIN { FS = ":" } {rec=$0 } END { print rec }' /etc/passwd
```
<!--ea-->

- Afficher la somme des chiffres de la 3ième colonne du fichier suivant :

```bash
  ~$ cat > numbers.txt << EOF
1 2 78
2 3 56
3 4 21
EOF
```

<!--ba-->
```bash
  ~$ awk 'BEGIN { FS = " "} {n=n+$3} END { print n }' numbers.txt
155
```
<!--ea-->

- Soit le fichier suivant : 

```bash
$ cat > data.txt << EOF
Alice:Merveille:Lastname1:0102030405
Bob:Sleigh:Lastname2:0506070809
Eve:Tentation:Lastname3:1011121314
EOF
```

Remplacer les : par des colonnes et les espacer par des tabulations.

<!--ba-->
```bash
  ~$ awk -F: '{ printf "%-5s %9s %8s %10s\n", $1, $2, $3, $4 }' data.txt > data.out.txt
  ~$ cat data.out.txt
Alice Merveille Lastname 0102030405
Bob      Sleigh Lastname 0506070809
Eve   Tentation Lastname 1011121314
155
```
<!--ea-->

- Afficher le contenu du fichier **liste.csv** avec **awk**

<!--ba-->
```bash
  ~$ awk '{print $0}' liste.csv |more 
sexe,preusuel,annais,nombre
1,A,1980,3.0000
1,A,1998,3.0000
1,A,XXXX,21.0000
1,AADEL,1976,5.0000
(...)
```
<!--ea-->

- Afficher la 2ième colonne du fichier **liste.csv**

<!--ba-->
```bash
  ~$ awk -F',' '{print $2}' liste.csv |more 
preusuel
A
A
A
AADEL
AADEL
AADEL
(...)
```
<!--ea-->

- Afficher les prénoms usuels déclarés en 1978

<!--ba-->
```bash
  ~$ awk -F',' '{ if ($3 == "1978") print $2}' liste.csv |more 
AADEL
AADIL
AAZIZ
ABBES
ABD
(...)
```
<!--ea-->

- Transformer le .csv en .txt en remplaçant les séparateurs avec virgule par une tabulation

<!--ba-->
```bash
  ~$ awk '$0=gensub(",","\t","g")' liste.csv > liste.txt
```
<!--ea-->


- Appliquer le script [norme.awk](norme.awk) au fichier [job.out](job.out)

<!--ba-->
```bash
  ~$ cat norme.awk
# evolution norme du champ de vitesse
BEGIN { np=0
  temps=0
  print "# evolution de la norme de la vitesse (it,t,u,u1,u2,u3)"
}
{
 if ($1 == "Iteration") temps=$5
 if (($1 == "Norme=")&&($3 == "u1=")) {
  np=np+1
  print np,temps,$2,$4,$6,$8
 }
}
END {
}
```
<!--ea-->

<!--ba-->
```bash
  ~$ awk -f norme.awk job.out > norme.dat
  ~$ cat norme.dat
# evolution de la norme de la vitesse (it,t,u,u1,u2,u3)
1 0.0008 10.4153 0.0837154 0.00649602 10.415
2 0.0016 10.4153 0.0842589 0.0122271 10.415
3 0.0024 10.4153 0.0850735 0.0175398 10.4149
4 0.0032 10.4153 0.0860854 0.0224537 10.4149
5 0.004 10.4153 0.087235 0.027 10.4149
6 0.0048 10.4153 0.0884743 0.0312068 10.4149
7 0.0056 10.4153 0.0897651 0.0351 10.4148
8 0.0064 10.4153 0.0910778 0.0387036 10.4148
9 0.0072 10.4153 0.0923897 0.0420396 10.4148
10 0.008 10.4153 0.0936837 0.0451286 10.4148
```
<!--ea-->


- Appliquer le script [inlet_analysis.sh](inlet_analysis.sh) au fichier [injection_SP2.inj](injection_SP2.inj)


<!--ba-->
```bash
  ~$ cat inlet_analysis.sh
#!/bin/bash

# Author : Paolo Errante, PfD, LMFA 

# remove old files
file=`ls . | grep "droplet_verif"`
for old in $file; do 
  rm -f $old
   echo "clean old files"
done

# setup output dir
dir='comparisons'
if [ ! -d $dir ]; then
   mkdir $dir
   echo "output dir $dir created"
fi


#Loops over radial positions to calculate mean and rms of axial and velocities + droplet D10 diameter at each position
for r in 0 0.00063 0.0012705 0.0019005 0.002541 0.003171 0.0038115 0.0044415 0.005082; do
  count=$(awk -v r=$r '{if($2==r) sum += 1}END{print sum}' injection_SP2.inj) #counts the number of droplets
  mean=$(awk -v r=$r -v count=$count '{if($2==r) sum += $4}END{print sum/count}' injection_SP2.inj) #calculate mean
  rms=$(awk -v r=$r -v count=$count -v mean=$mean '{if($2==r) sum += (mean - $4)^2}END{print sqrt(sum/count)}' injection_SP2.inj) #calculate rms
  echo "$r $mean $rms" >> droplet_verif_u.txt #print results to a file
done

for r in 0 0.00063 0.0012705 0.0019005 0.002541 0.003171 0.0038115 0.0044415 0.005082; do
  count=$(awk -v r=$r '{if($2==r) sum += 1}END{print sum}' injection_SP2.inj)
  mean=$(awk -v r=$r -v count=$count '{if($2==r) sum += $5}END{print sum/count}' injection_SP2.inj)
  rms=$(awk -v r=$r -v count=$count -v mean=$mean '{if($2==r) sum += (mean - $5)^2}END{print sqrt(sum/count)}' injection_SP2.inj)
  echo "$r $mean $rms" >> droplet_verif_v.txt
done

for r in 0 0.00063 0.0012705 0.0019005 0.002541 0.003171 0.0038115 0.0044415 0.005082; do
  count=$(awk -v r=$r '{if($2==r) sum += 1}END{print sum}' injection_SP2.inj)
  mean=$(awk -v r=$r -v count=$count '{if($2==r) sum += $7}END{print sum/count}' injection_SP2.inj)
  rms=$(awk -v r=$r -v count=$count -v mean=$mean '{if($2==r) sum += (mean - $7)^2}END{print sqrt(sum/count)}' injection_SP2.inj)
  echo "$r $mean $rms" >> droplet_verif_d.txt
done


column -t droplet_verif_u.txt > $dir/droplet_u_inlet.txt
column -t droplet_verif_v.txt > $dir/droplet_v_inlet.txt
column -t droplet_verif_d.txt > $dir/droplet_d_inlet.txt
```
<!--ea-->

<!--ba-->
```bash
  ~$ ./inlet_analysis.sh 
clean old droplet_verif_d.txt file
clean old droplet_verif_u.txt file
clean old droplet_verif_v.txt file
  ~$ cat comparisons/droplet_u_inlet.txt 
0          44.89    2.35064
0.00063    44.9487  2.43483
0.0012705  44.239   2.64825
0.0019005  43.3916  2.97924
0.002541   42.5012  3.43331
0.003171   38.1524  5.88757
0.0038115  32.3212  7.56589
0.0044415  26.6641  7.40637
0.005082   14.324   5.47973
```
<!--ea-->




# Bash

Dans un script bash, toutes les expressions et les variables sont des chaînes de caractère. 
Pour réaliser des opérations arithmétiques ou booléennes, il faut faire appel à des commandes spécficiques :
- **expr** (calcul)
- **test** (évaluation type booléen)

Le shell fournit 
- un mécanisme de passage de paramètres
- des structures de contrôle (de type if-then-else, for, etc.)
- un mécanisme de gestion de fonctions
Dans le cadre de projets de calcul scientifique, ce langage interprêté est généralement utilisé pour manipuler les fichiers (renommage, ect.), préparer les données (mise en forme, etc.) et automatiser (et rendre reproductible) les différentes manipulations effectuées dans le cadre du projet. 

### Exercices de base

- Écrire un script qui affiche le chemin du répertoire courant 

<!--ba-->
```bash
  ~$ cat << EOF >> pwd.sh
#!/bin/bash

pwd

EOF
  ~$ . ./pwd.sh
  ~$ chmod +x pwd.sh
  ~$ ./pwd.sh
```
<!--ea-->

- Écrire un script arg.sh qui retourne le nom du script, le nombre d'arguments et leurs valeurs et affiche le premier argument. Par exemple, la sortie de 

<!--ba-->
```bash
  ~$ ./arg.sh myvar1 myvar2
```
<!--ea-->

serait
<!--ba-->
```bash
  script name: ./arg.sh
  number of arguments: 2
  list of arguments: 
  argument: myvar
```
<!--ea-->

Réponse : 

<!--ba-->
```bash
  ~$ cat << EOF >> arg.sh
#!/bin/bash

echo "script name: "$0 
echo "number of arguments: "$#
echo "list of arguments: "$*
echo "fist argument: "$1
EOF
```
<!--ea-->

- Écrire un script qui renvoie le numéro du processus lancé (PID).

<!--ba-->
```bash
  ~$ cat << EOF >> num_proc.sh
#!/bin/bash

echo "process: "$$
EOF
```
<!--ea-->

### Exercices sur les structures de contrôle

- Écrire une boucle qui compte de 0 à 5

<!--ba-->
```bash
  ~$ cat << EOF >> count.sh
#!/bin/bash

for ((i=0; i <= 5 ; i++))
do
  echo "i = "$i
done

EOF
```
<!--ea-->


- Écrire un script qui teste sur le nombre d'argument du script est nul

<!--ba-->
```bash
  ~$ cat << EOF >> test_arg.sh
#!/bin/bash

if test $# -eq 0
then
  echo "no parameters"
else
  echo "number of parameters larger than 0"
fi
EOF
```
<!--ea-->

- Écrire un script qui renvoie une phrase qui explique la manière dont le script doit être lancé s'il n'a pas pris en compte d'argument

<!--ba-->
```bash
  ~$ cat << EOF >> print_help.sh
#!/bin/bash

[ $# -eq 0 ] && { echo "Usage: $0 arg1 arg2" ; exit 1; }

EOF
```
<!--ea-->

- Écrire un script qui illustre la différence entre $* et *@.

<!--ba-->
```bash
  ~$ cat << EOF >> arg_at_star.sh
#!/bin/bash

[ $# -eq 0 ] && { echo "Usage: $0 arg1 arg2" ; exit 1; }

echo "Using \"\$*\":"
for a in "$*"; do
      echo "<$a>";
done

echo "Using \"\$@\":"
for a in "$@"; do
      echo "<$a>";
done
EOF
```
<!--ea-->

- Écrire un script qui demande à l'utilisateur de saisir le mot OUI quelle que soit la case et le remercier.

<!--ba-->
```bash
  ~$ cat << EOF >> oui.sh
#!/bin/bash

echo "Enter OUI"
read reponse

case ${reponse} in
  OUI)  echo "Thank you very much!" ;;
  [Oo][Uu][Ii]) echo "Thank you" ;;
  o*|O*) echo "You are almost there!"
         echo "Would you please try again?" ;;
  n*|N*) echo "Why on earth?" ;;
  *)  echo "Shocking!"
      echo "Please try again" ;;
esac
EOF
```
<!--ea-->

- Écrire une boucle qui balaye tous les éléments de la liste suivante *un deux trois quatre*

<!--ba-->
```bash
  ~$ cat << EOF >> loop.sh
#!/bin/bash

for num in un deux trois quatre
do 
  echo "num= "$num
done
EOF
```
<!--ea-->

- Écrire une boucle qui liste tous les fichiers de type _.sh_ du répertoire courant 

<!--ba-->
```bash
  ~$ cat << EOF >> look_for_sh.sh
#!/bin/bash

for file in *.sh
do
  echo $file
done
EOF
```
<!--ea-->

- Écrire un programme qui affiche les 2 premières lignes de tous les fichiers de type _.sh_ du répertoire courant

<!--ba-->
```bash
  ~$ cat << EOF >> print_lines_from_sh.sh
#!/bin/bash

for myfile in *.sh
do
  echo "< file ${myfile} >"
  if [ ! -r "${myfile}" ]
  then
    echo "could not read ${myfile}"
    continue
  fi
  head -2 ${myfile}
done
exit 0


EOF
```
<!--ea-->

- Créer des fichiers (non vide)
    * LU.F90 
    * GAUSS.F90 
    * MAIN.F90 
    * main.f90
    * GaUSS.F90

avec la commande cat.

Écrire un programme qui passe tous ces noms de fichier en minuscule

<!--ba-->
```bash
cat << EOS >> make_file.sh 
#!/bin/bash

cat << EOF >> LU.F90
! LU solver
EOF

cat << EOF >> GAUSS.F90
! GAUSS solver
EOF

cat << EOF >> MAIN.F90
! MAIN file
EOF

cat << EOF >> GaUSS.F90
! GaUSS solver
EOF

cat << EOF >> main.F90
! main.F90
EOF

cat << EOF >> main.f90
! main.f90
EOF
EOS
``` 
<!--ea-->

Une première solution consiste à faire simplement une boucle sur les fichiers 

<!--ba-->
```bash
  ~$ cat << EOF >> put_min.sh
#!/bin/bash

for NAME in *.F90
do
  #echo "file= "${NAME}
  name=$(echo ${NAME} | tr '[:upper:]' '[:lower:]')
  #echo "file= "${name}
  mv ${NAME} ${name} && echo ${NAME} '=>' ${name}
done
echo "done"

exit

EOF
``` 
<!--ea-->

Attention, avec ce script, s'il existe des fichiers en minuscule du même nom, ils seront écrasés.  

- Modifier le script pour faire un test sur l'exsitance d'un fichier en minuscule et ne rien faire s'il existe.

<!--ba-->
```bash
  ~$ cat << EOF >> put_min_test.sh
#!/bin/bash

for NAME in *.F90
do
  if [ -f "${NAME}" ]
  then
    name=$(echo ${NAME} | tr '[:upper:]' '[:lower:]')
    if [ "${name}" != "${NAME}" ]
    then
      if [ -s "${name}" ]
      then 
        # lower capital file already exists and is not empty
        echo ${NAME} will erase ${name} '=>' escape >&2
      else 
        mv ${NAME} ${name} && echo ${NAME} '=>' ${name}
      fi
    fi
  else
    # original file not a regular file
    echo "${NAME} not a regular file => escape" >&2
  fi
done
echo "done"
exit

EOF
``` 
<!--ea-->

- Écrire un script qui compte le nombre de fichiers de type f90 dans le répertoire, avec **expr** (ou avec **let**)


<!--ba-->
```bash
  ~$ cat << EOF >> count_f90_let.sh
#!/bin/bash

n=0
s
for file in *.f90
do
  let n+=1
done 
echo "There is $n .f90 files"

EOF
``` 
<!--ea-->



<!--ba-->
```bash
  ~$ cat << EOF >> count_f90_expr.sh
#!/bin/bash

n=0
for file in *.f90
do
  n=$(expr $n + 1)
  echo 'n= '$n
done
echo "There is $n .f90 files"

EOF
``` 
<!--ea-->

- Écrire un script faisant la somme, le dfférence, la multiplication, la division de 2 et 3.

<!--ba-->
```bash
  ~$ cat << EOF >> arithmetic.sh
#!/bin/bash

u=2
v=3

w=`expr $u + $v`
echo '2+3 = '$w

w=`expr $u - $v`
echo '2-3 = '$w

w=`expr $u \* $v`
echo '2*3 = '$w

w=`expr $u / $v`   
echo '2/3 = '$w

EOF
``` 
<!--ea-->




### Exercices sur les fonctions

- Écrire un script qui renvoie une aide sur l'usage de ce script lorsque l'argument **-h** lui est passé. L'aide est décrite dans une fonction.

<!--ba-->
```bash
  ~$ cat << EOF >> exemple_help.sh
#!/bin/bash

usage()
{
    echo "Usage: $0 [-f filename -h | --help]"
    exit 1
}
warn()
{
    echo "the filename in argument is prefixed after -f"
    usage
}


[ $# -eq 0 ] && { warn; exit 1; }


#echo "number of arguments: "$#
#echo "arguments: "$*

while [ "$1" != "" ]; do
    case $1 in
         -f | --file )        shift
                              filename=$1
                              ;;
         -h | --help )        usage
                              ;;
         * )                  warn
                              exit 1
    esac
    shift
    echo "Filename: "$filename
done
EOF
```
<!--ea-->





# Connexion distante

## ssh

- Se connecter avec ssh sur une machine distante, par exemple sur newton, dont l'adresse est newton-login.pmcs2i.ec-lyon.fr

<!--ba-->
```bash 
  ~$ ssh acadiou@newton-login.pmcs2i.ec-lyon.fr
Passwd :
acadiou@newton-login$
```
<!--ea-->

- Générer une clef ssh 

<!--ba-->
```bash 
  ~$ ssh-keygen -t rsa
```
<!--ea-->

- Copier la clef ssh sur la machine distante

<!--ba-->
```bash 
  ~$ ssh-copy-id -i ~/.ssh/id_rsa acadiou@newton-login.pmcs2i.ec-lyon.fr
```
<!--ea-->

- Se connecter à nouveau sur la machine distante 

<!--ba-->
```bash 
  ~$ ssh acadiou@newton-login.pmcs2i.ec-lyon.fr
Passwd :
acadiou@newton-login$
```
<!--ea-->

- Configurer ~/.ssh/config pour mettre un alias sur newton-login.pmcs2i.ec-lyon.fr (par exemple newton-login)

<!--ba-->
```bash 
  ~$ cat << EOF >> ~/.ssh/config
Host newton-login
  Hostname login.pmcs2i.ec-lyon.fr
```
<!--ea-->


- Configurer ~/.ssh/config pour mettre un alias sur la connexion à newton-compil (accessible depuis newton-login) en passant par newton-login

<!--ba-->
```bash 
  ~$ cat << EOF >> ~/.ssh/config
Host newton newton-compil
  Hostname compil
  User  acadiou
  ProxyCommand  ssh acadiou@newton-login -W %h:%p
```
<!--ea-->























