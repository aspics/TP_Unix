# evolution norme du champ de vitesse
BEGIN { np=0
	temps=0
	print "# evolution de la norme de la vitesse (it,t,u,u1,u2,u3)"
}
{
 if ($1 == "Iteration") temps=$5
 if (($1 == "Norme=")&&($3 == "u1=")) {
	np=np+1
	print np,temps,$2,$4,$6,$8
 }
}
END {
}
